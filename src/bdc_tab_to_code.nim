import os
import std/strutils

# This program generates code for SAP BDC recordings.
# It takes the standard output .txt file of the 
# SAP transaction SHDB and generates code that creates
# the BDC table (type table of bdcdata) and calls the
# transaction (if there is one on top of the BDC table).
# The generated code is output to stdout and a file in
# the directory of the exectuable.

# SAP type table of bdcdata
#program  bdc_prog char(40) BDC module pool 
#dynpro  bdc_dynr numc(4) BDC Dynpro Number 
#dynbegin  bdc_start char(1) BDC Dynpro Start 
#fnam  fnam_____4 char(132) Field name 
#fval  bdc_fval char(132) BDC field value
type
  BDCLine = object 
    program: string
    dynpro: string
    dynbegin: string
    fnam: string
    fval: string

const 
  default_file_name = "bdc_recording.txt"
  help_arg = "--help"
  help_short_arg = "-h"
var file_path = getCurrentDir() & "\\" & default_file_name

proc get_file_path(file_path: var string) =
  while not fileExists(file_path):
    echo "file ", default_file_name, 
      " does not exist in path of executable: ", getCurrentDir()
    echo "please provide bdc file location manually: " 
    file_path = readLine(stdin)

proc build_bdc_sequence(file_path: var string): seq[BDCLine] =
  get_file_path(file_path)
  echo "reading file: ", file_path
  let file = open(file_path)
  while not endOfFile(file):
    let 
      line = readLine(file)
      vals = line.split('\t')
    var 
      idx = 0
      bdc_line: BDCLine
      one_val_found = false
    for name, val in bdc_line.fieldPairs():
      if vals.high < idx: break
      if not (vals[idx].isEmptyOrWhitespace() or vals[idx] == "0000"):
        one_val_found = true
        val = vals[idx].strip()
      inc idx
    if one_val_found: result.add(bdc_line)

proc code_gen(bdc_lines: seq[BDCLine]): string =
  result = "TYPES: tt_bdc_data TYPE TABLE OF bdcdata WITH DEFAULT KEY.\n"
  var entry_transaction: string
  if (bdc_lines.len >= 1) and (bdc_lines[0].dynbegin == "T"):
    entry_transaction = bdc_lines[0].fnam
  result &= "\nDATA(lt_bdc) = VALUE tt_bdc_data(\n"
  for idx, line in bdc_lines:
    if line.dynbegin == "T":
      result &= "  \" call transaction " & line.fnam & "\n"
    result &= "  ("
    for name, val in line.fieldPairs:
      if val != "":
        result &= "\n    " & name & " = \'" & val & "\'"
    result &= "\n  )\n"
  result &= ").\n"
  if entry_transaction != "":
    result &= 
      "\nTRY.\n" & 
      "\n    DATA lt_msg TYPE TABLE OF bdcmsgcoll WITH DEFAULT KEY.\n" & 
      "\n    CALL TRANSACTION \'" & entry_transaction & "\'\n" &
      "      USING lt_bdc\n" & 
      "      MODE 'E'\n" & 
      "      UPDATE 'S'\n" & 
      "      MESSAGES INTO lt_msg.\n" &
      "\n  CATCH cx_sy_authorization_error.\n" & 
      "    \" TODO handle authorization error\n" & 
      "    ROLLBACK WORK.\n" &
      "ENDTRY.\n" &
      "\nDATA(lv_idx) = line_index( lt_msg[ msgtyp = 'A' ] ).\n" &
      "IF lv_idx EQ 0.\n" &
      "  lv_idx = line_index( lt_msg[ msgtyp = 'E' ] ).\n" &
      "ENDIF.\n" &
      "\nIF lv_idx GT 0.\n" &
      "\n  ASSIGN lt_msg[ lv_idx ] TO FIELD-SYMBOL(<fs_msg>).\n" &
      "  \" TODO handle error messages\n" &
      "\nENDIF."

proc write_generated_code_to_file(output_code: string) =
  let new_file_path = getCurrentDir() & "\\generated_code.txt"
  writeFile(new_file_path, output_code)
  echo "wrote generated code to: ", new_file_path

proc help =
  echo "\tProvide the path to the file you want to generate code for as" &
    " the first argument. \n\tIf you provide no argument, the default file path\n\t\"" &
    file_path & "\"\n\twill be assumed (file \"" & default_file_name & "\" is assumed to" &
    " be in the same directory as this executable. \n\tIf it is not found, you " &
    "will be promted to enter a valid file location."

when isMainModule:
  if paramCount() > 0:
    if (paramStr(1) == help_arg) or (paramStr(1) == help_short_arg):
      help()
      quit(1)
    else:
      file_path = paramStr(1)
  else:
    echo "no argument provided, assuming defualt file name and location:"
    echo "\"" , file_path
  let 
    bdc_lines = build_bdc_sequence(file_path)
    output_code = code_gen(bdc_lines)
  if output_code != "":
    echo "generated code: \n----------------------------------------\n"
    echo output_code
    echo "----------------------------------------"
    write_generated_code_to_file(output_code)