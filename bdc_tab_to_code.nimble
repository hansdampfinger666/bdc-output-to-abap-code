# Package

version       = "0.1.0"
author        = "="
description   = "Generate code from BDC txt file"
license       = "GPL-2.0-or-later"
srcDir        = "src"
bin           = @["bdc_tab_to_code"]


# Dependencies

requires "nim >= 2.0.0"